package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Another change
 * 
 * Change A
 * Change B
 * Change C
 * Change XY
 * 
 * Version 2
 * 
 * @author Vít Herain
 *
 */
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
    	// Fixed the bug
        SpringApplication.run(DemoApplication.class, args);
    }
    // Change D
}
